package module.payment.localdb.productinfo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class ProductListFragment extends Fragment {
    private RecyclerView recyclerView;
    private ProductsAdapter mAdapter;
    private SQLiteDatabase db;
    private List<String> pname = new ArrayList<>();
    private List<String> pprice = new ArrayList<>();
    private List<String> punit = new ArrayList<>();
    private Listdata listdata;
    private List<Listdata> list = new ArrayList<>();
    private Context context;

    public ProductListFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ProductListFragment(Context ctx) {
        context = ctx;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_product_list, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_showproducts);
        db = context.openOrCreateDatabase(BaseApplication.DB_NAME, MODE_PRIVATE, null);
        db.execSQL(BaseApplication.CREATE_TABLE_QUERY);


        mAdapter = new ProductsAdapter(list, context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        fillList();


        return view;
    }


    public void fillList() {
        try {
            Cursor cursor = db.rawQuery("Select * From tbl_products Order By pname ASC", null);
            cursor.moveToLast();
            int cnt = cursor.getCount();
            cursor.moveToFirst();
            if (cnt != 0) {
                do {
                    listdata = new Listdata();
                    listdata.setPname(cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMN_PRODUCT_NAME)));
                    listdata.setPprice(cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMN_PRODUCT_PRICE)));
                    listdata.setPunits(cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMN_PRODUCT_UNIT)));
                    listdata.setDateTime(cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMNE_DATE_TIME)));
                    listdata.setID(cursor.getString(cursor.getColumnIndex("pid")));
                    list.add(listdata);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.getMessage();
        }
        mAdapter.notifyDataSetChanged();
    }
}
