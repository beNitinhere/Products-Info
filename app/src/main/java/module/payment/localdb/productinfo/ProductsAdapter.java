package module.payment.localdb.productinfo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {
    List<Listdata> list = new ArrayList<>();
    Context context;
    SQLiteDatabase db;

    public ProductsAdapter() {
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView sno, product_name, product_price, product_unit;

        public MyViewHolder(View view) {
            super(view);
            sno = (TextView) view.findViewById(R.id.recycler_sno);
            product_name = (TextView) view.findViewById(R.id.recycler_name);
            product_price = (TextView) view.findViewById(R.id.recycler_price);
            product_unit = (TextView) view.findViewById(R.id.recycler_unit);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("RecyclerView", "onClick：" + getAdapterPosition());
                    String str_pname = product_name.getText().toString();
                    showMenu(str_pname, v, getAdapterPosition());
                }
            });
        }
    }

    public ProductsAdapter(List<Listdata> list, Context ctx) {
        this.list = list;
        context = ctx;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Listdata listdata1 = list.get(position);
        holder.sno.setText(listdata1.getDateTime());
        holder.product_name.setText(listdata1.getPname());
        holder.product_price.setText(listdata1.getPprice() + " Rs.");
        holder.product_unit.setText(listdata1.getPunits());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void showMenu(final String str_pname, final View v, final int adapterPosition) {
        PopupMenu popup = new PopupMenu(context, v);
        final View view = v;

        // This activity implements OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.popup_edit:
                        editProduct(str_pname, view, adapterPosition);
                        return true;
                    case R.id.popup_delete:
                        deleteProduct(view, adapterPosition);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.inflate(R.menu.list_popup);
        popup.show();
    }

    private void editProduct(String str_pname, View v, int position) {
        final Listdata listdata = list.get(position);
        final LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.edit_productdetails, null);


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        alertDialogBuilder.setView(promptsView);

        final EditText pname = (EditText) promptsView
                .findViewById(R.id.edt_pop_pname);

        final EditText pprice = (EditText) promptsView
                .findViewById(R.id.edt_pop_product_price);


        final Spinner spinner_unit = (Spinner) promptsView.findViewById(R.id.product_pop_unit_spinner);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, R.layout.support_simple_spinner_dropdown_item, BaseApplication.units);
        spinner_unit.setAdapter(arrayAdapter);

        db = context.openOrCreateDatabase(BaseApplication.DB_NAME, MODE_PRIVATE, null);
        db.execSQL(BaseApplication.CREATE_TABLE_QUERY);

        Cursor cursor = db.rawQuery("Select * From tbl_products Where pname='" + str_pname + "'", null);
        cursor.moveToLast();
        int cnt = cursor.getCount();
        cursor.moveToFirst();
        if (cnt != 0) {
            do {
                pname.setText(cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMN_PRODUCT_NAME)));
                pprice.setText(cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMN_PRODUCT_PRICE)));
                String string_unit = cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMN_PRODUCT_UNIT));
                for (int i = 0; i < BaseApplication.units.length; i++) {
                    if (BaseApplication.units[i].equals(string_unit)) {
                        spinner_unit.setSelection(i);
                        break;
                    }
                }

            } while (cursor.moveToNext());
        }
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String str_pname = pname.getText().toString();
                                String str_pprice = pprice.getText().toString();
                                String str_punit = spinner_unit.getSelectedItem().toString();

                                if (str_pname.isEmpty() || str_pprice.isEmpty() || str_punit.equals("Select unit"))
                                    Toast.makeText(context, "Product fields can't be empty", Toast.LENGTH_SHORT).show();
                                else {
                                    Calendar cal = Calendar.getInstance();
                                    String date = "" + cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR);

                                    String sql = "Update tbl_products Set pname='" + str_pname + "' , pprice='" + str_pprice + "' , punit='" + str_punit + "' , pdatetime='" + date + "' Where pid='" + listdata.getID() + "' ";
                                    db = context.openOrCreateDatabase(BaseApplication.DB_NAME, MODE_PRIVATE, null);
                                    db.execSQL(BaseApplication.CREATE_TABLE_QUERY);
                                    db.execSQL(sql);
                                    Toast.makeText(context, "Product details updated successfully", Toast.LENGTH_SHORT).show();
                                    listdata.setDateTime(date);
                                    listdata.setPname(str_pname);
                                    listdata.setPprice(str_pprice);
                                    listdata.setPunits(str_punit);
                                    notifyDataSetChanged();
                                }

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    private void deleteProduct(View v, final int position) {
        final Listdata listdata = list.get(position);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        alertDialogBuilder.setMessage("Do you wanna delete product ?");
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String sql = "Delete From tbl_products Where pid='" + listdata.getID() + "' ";
                db = context.openOrCreateDatabase(BaseApplication.DB_NAME, MODE_PRIVATE, null);
                db.execSQL(BaseApplication.CREATE_TABLE_QUERY);
                db.execSQL(sql);
                Toast.makeText(context, "Product deleted successfully", Toast.LENGTH_SHORT).show();
                list.remove(position);
                notifyItemRemoved(position);
                notifyDataSetChanged();
            }
        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).setCancelable(false);
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


    }

}

