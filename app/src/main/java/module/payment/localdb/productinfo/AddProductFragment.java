package module.payment.localdb.productinfo;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class AddProductFragment extends Fragment implements View.OnClickListener {
    private SQLiteDatabase db;
    private String strPname, strPprice, strUnit;
    EditText edt_pname, edt_pprice;
    Spinner spinner;
    Context context;


    public AddProductFragment() {
    }

    @SuppressLint("ValidFragment")
    public AddProductFragment(Context ctx) {
        context = ctx;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_product, container, false);

        db = context.openOrCreateDatabase(BaseApplication.DB_NAME, MODE_PRIVATE, null);
        db.execSQL(BaseApplication.CREATE_TABLE_QUERY);
        edt_pname = (EditText) view.findViewById(R.id.edt_product_name);
        edt_pprice = (EditText) view.findViewById(R.id.edt_product_price);
        view.findViewById(R.id.btn_add).setOnClickListener(this);
        spinner = (Spinner) view.findViewById(R.id.product_unit_spinner);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, R.layout.support_simple_spinner_dropdown_item, BaseApplication.units);
        spinner.setAdapter(arrayAdapter);
        // Inflate the layout for this fragment
        return view;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_add:
                strPname = edt_pname.getText().toString();
                strPprice = edt_pprice.getText().toString();
                strUnit = spinner.getSelectedItem().toString();
                saveToDatabase(strPname, strPprice, strUnit);
                edt_pprice.setText("");
                edt_pname.setText("");
                break;

        }
    }

    public void saveToDatabase(String pname1, String pprice1, String punit1) {
        try {
            if (!pname1.isEmpty() && !pprice1.isEmpty()) {
                String sql = "Select * From tbl_products Where pname = '" + pname1 + "'";
                Cursor cursor = db.rawQuery(sql, null);
                cursor.moveToLast();
                int cnt = cursor.getCount();
                cursor.moveToFirst();
                if (cnt == 0) {
                    Calendar cal = Calendar.getInstance();

                    String date = "" + cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR);

                    db.execSQL("Insert Into tbl_products(pname,pprice,punit,pdatetime)" +
                            " Values('" + pname1 + "','" + pprice1 + "','" + punit1 + "','" + date + "')");
                    Toast.makeText(context, "Product saved", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(context, "Product already saved", Toast.LENGTH_SHORT).show();

                cursor.close();
            } else
                Toast.makeText(context, "Product fields can't be empty", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("SQL Error", e.getMessage());
        }
    }




    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
