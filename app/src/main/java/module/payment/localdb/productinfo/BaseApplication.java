package module.payment.localdb.productinfo;

public class BaseApplication {
    public static final String DB_NAME = "db_Products";
    public static final String TABLE_NAME = "tbl_products";
    public static final String COLUMN_PRODUCT_NAME="pname";
    public static final String COLUMN_PRODUCT_PRICE="pprice";
    public static final String COLUMN_PRODUCT_UNIT="punit";
    public static final String COLUMNE_DATE_TIME="pdatetime";
    public static final String CREATE_TABLE_QUERY = "Create Table If Not Exists tbl_products(pname nvarchar(20),punit nvarchar(10),pprice nvarchar(20),pid Integer Primary Key AutoIncrement,pdatetime nvarchar(30))";
    public  static final String units[] = {"Kg", "Ltr", "Unit"};
}
