package module.payment.localdb.productinfo;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class CartFragment extends Fragment {
    private SQLiteDatabase db;
    private String strPname, strPprice, strUnit;
    TextView txt_pquantity;
    static TextView txt_totalprice;
    EditText edt_pquantity;
    Spinner spinner;
    Context context;
    List<String> listpname = new ArrayList<>();
    List<String> listpunit = new ArrayList<>();
    List<String> listpprice = new ArrayList<>();
    CartAdapter mAdapter;
    RecyclerView recyclerView;
    List<CartData> list = new ArrayList<>();
    CartData cartData;
    AppCompatButton saveButton, generatebill_;
    CartData cartData12 = new CartData();
    static int total;

    public CartFragment() {
    }

    @SuppressLint("ValidFragment")
    public CartFragment(Context ctx) {
        context = ctx;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);

        db = context.openOrCreateDatabase(BaseApplication.DB_NAME, MODE_PRIVATE, null);
        db.execSQL(BaseApplication.CREATE_TABLE_QUERY);
        edt_pquantity = (EditText) view.findViewById(R.id.edt_product_quantity);
        spinner = (Spinner) view.findViewById(R.id.spinner_pname);
        txt_pquantity = (TextView) view.findViewById(R.id.txt_punit);
        txt_totalprice = (TextView) view.findViewById(R.id.txt_totalprice);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_showproducts);
        saveButton = (AppCompatButton) view.findViewById(R.id.cart_save);
        generatebill_ = (AppCompatButton) view.findViewById(R.id.btn_generate_bill);
        fillSpinnerArray();


        mAdapter = new CartAdapter(list, context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, R.layout.support_simple_spinner_dropdown_item, listpname);
//        spinner.setAdapter(arrayAdapter);

        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, listpname, listpprice);
        spinner.setAdapter(spinnerAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String str_selected_item_unit = listpunit.get(position);
                txt_pquantity.setText(str_selected_item_unit);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fillList();
            }
        });

        generatebill_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateBill();
            }
        });

        return view;
    }

    public void fillSpinnerArray() {
        String sql = "Select * From tbl_products Order By pname ASC";
        db = context.openOrCreateDatabase(BaseApplication.DB_NAME, MODE_PRIVATE, null);
        db.execSQL(BaseApplication.CREATE_TABLE_QUERY);
        Cursor cursor = db.rawQuery(sql, null);
        cursor.moveToLast();
        int cnt = cursor.getCount();
        cursor.moveToFirst();
        if (cnt != 0) {
            do {
                String pname = cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMN_PRODUCT_NAME));
                String punit = cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMN_PRODUCT_UNIT));
                String pprice = cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMN_PRODUCT_PRICE));
                listpname.add(pname);
                listpunit.add(punit);
                listpprice.add(pprice);
//                mapprice.put(pname, pprice);
            } while (cursor.moveToNext());
        }

    }

    public void setTotal(int int_total) {
        if (list.size() == 1) total = 0;
        total += int_total;
        txt_totalprice.setText(total + "");

    }

    public static void setTotalChange(int int_total) {
        total -= int_total;
        txt_totalprice.setText(total + "");
    }

    public void fillList() {
        String str_pname, str_pquantity;
        str_pname = spinner.getSelectedItem().toString();
        str_pquantity = edt_pquantity.getText() + "";
        if (!str_pquantity.isEmpty()) {
            try {
                Cursor cursor = db.rawQuery("Select * From tbl_products Where pname='" + str_pname + "'", null);
                cursor.moveToLast();
                int cnt = cursor.getCount();
                cursor.moveToFirst();
                if (cnt != 0) {
                    int int_ptotal;
                    do {
                        cartData = new CartData();
                        cartData.setPname(cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMN_PRODUCT_NAME)));
                        cartData.setPprice(cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMN_PRODUCT_PRICE)));
                        cartData.setQuantity(str_pquantity);
                        String pprice = cursor.getString(cursor.getColumnIndex(BaseApplication.COLUMN_PRODUCT_PRICE));
                        int_ptotal = Integer.parseInt(pprice) * Integer.parseInt(str_pquantity);
                        cartData.setTotal(int_ptotal + "");
                        list.add(cartData);
                    } while (cursor.moveToNext());
                    edt_pquantity.setText("");
                    setTotal(int_ptotal);

                }
            } catch (Exception e) {
                e.getMessage();
            }
            mAdapter.notifyDataSetChanged();
        }
    }


    public void generateBill() {
        final LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.generate_bill, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        alertDialogBuilder.setView(promptsView);

        final RecyclerView recyclerView1 = (RecyclerView) promptsView
                .findViewById(R.id.recycler_bill_list);

        final TextView textView_alert
                = (TextView) promptsView
                .findViewById(R.id.txt_totalprice_1);


        mAdapter = new CartAdapter(list, context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerView1.setLayoutManager(mLayoutManager);
        recyclerView1.setItemAnimator(new DefaultItemAnimator());
        recyclerView1.setAdapter(mAdapter);
        textView_alert.setText(txt_totalprice.getText() + "");

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Done",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

}
