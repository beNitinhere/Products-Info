package module.payment.localdb.productinfo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    List<CartData> list = new ArrayList<>();
    Context context;
    SQLiteDatabase db;
    CartFragment cartFragment=new CartFragment();

    public CartAdapter() {
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_pname, txt_pprice, txt_pquantity, txt_ptotal;
        public CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            txt_pname = (TextView) view.findViewById(R.id.cart_pname);
            txt_pprice = (TextView) view.findViewById(R.id.cart_pprice);
            txt_pquantity = (TextView) view.findViewById(R.id.cart_pquantity);
            txt_ptotal = (TextView) view.findViewById(R.id.cart_ptotal);
            cardView = (CardView) view.findViewById(R.id.cart_cardView);
        }
    }

    public CartAdapter(List<CartData> list, Context ctx) {
        this.list = list;
        context = ctx;
    }


    @Override
    public CartAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_list, parent, false);

        return new CartAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CartAdapter.MyViewHolder holder, final int position) {
        CartData cartData = list.get(position);
        holder.txt_pname.setText(cartData.getPname());
        holder.txt_pprice.setText("Price :- " + cartData.getPprice() + " Rs.");
        holder.txt_pquantity.setText(cartData.getQuantity());
        holder.txt_ptotal.setText("Total price :- " + cartData.getTotal() + " Rs.");

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(v, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void deleteCard(View view, int position) {
        CartData cartData = list.get(position);
        int itemtotal = Integer.parseInt(cartData.getTotal());
        cartFragment.setTotalChange(itemtotal);
        Toast.makeText(context, "Product deleted successfully from your cart", Toast.LENGTH_SHORT).show();
        list.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();

    }

    public void showMenu(final View v, final int adapterPosition) {
        PopupMenu popup = new PopupMenu(context, v);
        final View view = v;

        // This activity implements OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.cart_popup_delete:
                        deleteCard(view, adapterPosition);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.inflate(R.menu.cart_deletemenu);
        popup.show();
    }
}
