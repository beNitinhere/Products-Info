package module.payment.localdb.productinfo;

public class CartData {
    String pname, pprice, ptotal, pquantity;

    public CartData(){}

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPname() {
        return pname;
    }

    public void setPprice(String pprice) {
        this.pprice = pprice;
    }

    public String getPprice() {
        return pprice;
    }

    public void setTotal(String ptotal) {
        this.ptotal = ptotal;
    }

    public String getTotal() {
        return ptotal;
    }

    public void setQuantity(String pquantity) {
        this.pquantity = pquantity;
    }

    public String getQuantity() {
        return pquantity;
    }



}

