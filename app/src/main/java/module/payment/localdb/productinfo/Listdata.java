package module.payment.localdb.productinfo;

public class Listdata {
    String pname, pprice, punits,pdatetime,pid;

    public void setPname(String pname) {
        this.pname = pname;
    }

    public void setPprice(String pprice) {
        this.pprice = pprice;
    }

    public void setPunits(String punits) {
        this.punits = punits;
    }

    public String getPname() {
        return pname;
    }

    public String getPprice() {
        return pprice;
    }

    public String getPunits() {
        return punits;
    }

    public void setDateTime(String pdatetime) {
        this.pdatetime = pdatetime;
    }

    public String getDateTime() {
        return pdatetime;
    }

    public void setID(String pid) {
        this.pid = pid;
    }

    public String getID() {
        return pid;
    }
}
