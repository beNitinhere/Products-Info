package module.payment.localdb.productinfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.List;

public class SpinnerAdapter extends ArrayAdapter<String> {
    List<String> array_ppname;
    List<String> array_pprice;
    Context mcontext;

    public SpinnerAdapter(Context context, List<String> array_ppname, List<String> array_pprice) {
        super(context, R.layout.products_with_price, R.id.plist_pname_adapter, array_ppname);
        this.array_ppname = array_ppname;
        this.array_pprice = array_pprice;
        mcontext = context;

    }

    @Override
    public View getView(final int pos, View v, ViewGroup parent) {
        View row = v;
        ViewHolder vhold = null;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(mcontext.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.products_with_price, parent, false);
            vhold = new ViewHolder(row);
            row.setTag(vhold);
        } else {
            vhold = (ViewHolder) row.getTag();
        }
        vhold.txt_pname.setText(array_ppname.get(pos));
        vhold.txt_pprice.setText(array_pprice.get(pos));
        return row;
    }

    class ViewHolder {
        TextView txt_pname, txt_pprice;

        public ViewHolder(View v) {
            txt_pname = (TextView) v.findViewById(R.id.plist_pname_adapter);
            txt_pprice = (TextView) v.findViewById(R.id.plist_pprice_adapter);

        }
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder2 holder;

        if (convertView == null) {
            holder = new ViewHolder2();
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(mcontext.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.products_with_price, null);
            holder.text1 = (TextView) convertView.findViewById(R.id.plist_pname_adapter);
            holder.text2 = (TextView) convertView.findViewById(R.id.plist_pprice_adapter);
            convertView.setTag(R.layout.products_with_price, holder);
        } else {
            holder = (ViewHolder2) convertView.getTag(R.layout.products_with_price);
        }

        holder.text1.setText(array_ppname.get(position));
        holder.text2.setText(array_pprice.get(position));

        return convertView;
    }


    static class ViewHolder2 {
        TextView text1;
        TextView text2;
    }
}
